require("babel-polyfill");
function getDevTool() {
  if (process.env.NODE_ENV !== "production") {
    return "source-map"; //enables source map
  }

  return false;
}

var config = {
  entry: ["babel-polyfill", "./src/main.js"], // entry point
  output: {
    filename: "./dist/index.js" // place where bundled app will be served
  },
  devServer: {
    inline: true, // autorefresh
    port: 8080 // development port server
  },
  devtool: getDevTool(),
  module: {
    loaders: [
      {
        test: /\.jsx?$/, // search for js files
        exclude: /node_modules/,
        loader: "babel-loader",
        query: {
          presets: ["es2015", "react", "stage-0"] // use es2015 and react
        }
      }
    ]
  }
};
module.exports = config;
