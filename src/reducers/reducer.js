import { data } from "../data.js";

const defaultState = {
  results: data.results,
  saved: data.saved
};

export const selectors = (() => {
  return {
    results: state => state.results,
    saved: state => state.saved,
  };
})();

export const addProperty = id => ({ type: "ADD_PROPERTY", id });
export const removeProperty = id => ({ type: "REMOVE_PROPERTY", id });

export const appReducer = (state = defaultState, action = {}) => {
  switch (action.type) {
    case "ADD_PROPERTY":
      const savedPropertyIds = state.saved.map(p => p.id);
      return {
        ...state,
        // check whether the property has already been saved 
        saved: savedPropertyIds.indexOf(action.id) === -1
         ? [...state.saved, state.results.filter(p => p.id == action.id)[0]]
         : state.saved
      };

    case "REMOVE_PROPERTY":
      return {
        ...state,
        saved: state.saved.filter(p => p.id !== action.id)
      };

    // Default
    default:
      return state;
  }
};
