import React from "react";
import App from "../components/App.jsx";
import Property from "../components/Properties/Property/property.jsx";
import Properties from "../components/Properties/Properties.jsx";
import Card from "../components/Properties/Property/_shared/Card.jsx";
import { data } from "../data.js";
import Enzyme, { shallow, mount } from "enzyme";
import Immutable from "seamless-immutable";
import { Reducer, Selector } from "redux-testkit";
import Adapter from "enzyme-adapter-react-16";
import configureStore from "redux-mock-store";
import { createStore } from "redux";
import {
  addProperty,
  removeProperty,
  appReducer,
  selectors
} from "../reducers/reducer.js";

Enzyme.configure({ adapter: new Adapter() });

const initialState = {
  results: data.results,
  saved: data.saved
};

const emptyState = Immutable({
  results: [],
  saved: []
});

const stateSavedTwo = Immutable({
  results: [
    {
      price: "$726,500",
      agency: {
        brandingColors: {
          primary: "#ffe512"
        },
        logo:
          "http://i1.au.reastatic.net/agencylogo/XRWXMT/12/20120927204448.gif"
      },
      id: "1",
      mainImage:
        "http://i2.au.reastatic.net/640x480/20bfc8668a30e8cabf045a1cd54814a9042fc715a8be683ba196898333d68cec/main.jpg"
    },
    {
      price: "$560,520",
      agency: {
        brandingColors: {
          primary: "#fcfa3b"
        },
        logo:
          "http://i4.au.reastatic.net/agencylogo/BFERIC/12/20150619122858.gif"
      },
      id: "2",
      mainImage:
        "http://i1.au.reastatic.net/640x480/88586227f9176f602d5c19cf06261108dbb29f03e30d1c4ce9fc2b51fb1e4bd6/main.jpg"
    },
    {
      price: "$826,500",
      agency: {
        brandingColors: {
          primary: "#57B5E0"
        },
        logo:
          "http://i1.au.reastatic.net/agencylogo/XCEWIN/12/20150807093203.gif"
      },
      id: "3",
      mainImage:
        "http://i4.au.reastatic.net/640x480/98cee1b2a3a64329921fc38f7e2926a78d41fcc683fc48fb8a8ef2999b14c027/main.jpg"
    }
  ],
  saved: [
    {
      price: "$526,500",
      agency: {
        brandingColors: {
          primary: "#000000"
        },
        logo:
          "http://i2.au.reastatic.net/agencylogo/WVYSSK/2/20140701084436.gif"
      },
      id: "4",
      mainImage:
        "http://i2.au.reastatic.net/640x480/5e84d96722dda3ea2a084d6935677f64872d1d760562d530c3cabfcb7bcda9c2/main.jpg"
    },
    {
      price: "$560,520",
      agency: {
        brandingColors: {
          primary: "#fcfa3b"
        },
        logo:
          "http://i4.au.reastatic.net/agencylogo/BFERIC/12/20150619122858.gif"
      },
      id: "2",
      mainImage:
        "http://i1.au.reastatic.net/640x480/88586227f9176f602d5c19cf06261108dbb29f03e30d1c4ce9fc2b51fb1e4bd6/main.jpg"
    }
  ]
});

// unit test for Card Component
describe("component/card", () => {
  let card;
  beforeEach(() => {
    card = shallow(<Card />);
  });

  it("should only display button on hover", () => {
    card.simulate("mouseEnter");
    expect(card.find(".actionButton").length).toEqual(1);
    card.simulate("mouseLeave");
    expect(card.find(".actionButton").length).toEqual(0);
  });
});

// unit test for Property Component
describe("component/property", () => {
  let property;
  const props = {
    addProperty: jest.fn()
  };
  beforeEach(() => {
    property = shallow(<Property {...props} />);
  });
  it("should only display proper text according to which function passed in", () => {
    expect(property.find(Card).props().buttonText).toBe("add property");
  });
});

// unit test for reducer
describe("store/reducer", () => {
  it("should have initial state", () => {
    expect(appReducer()).toEqual(initialState);
  });

  it("should not affect state", () => {
    Reducer(appReducer)
      .expect({ type: "NOT_EXISTING" })
      .toReturnState(initialState);
  });

  it("should be able to remove saved property ", () => {
    const action = { type: "REMOVE_PROPERTY", id: "4" };
    Reducer(appReducer)
      .expect(action)
      .toReturnState({ ...initialState, saved: [] });
  });

  it("should be able to add property", () => {
    const action = { type: "ADD_PROPERTY", id: "2" };
    Reducer(appReducer)
      .expect(action)
      .toReturnState(stateSavedTwo);
  });

  it("should not be able to add saved property again", () => {
    const action = { type: "ADD_PROPERTY", id: "4" };
    Reducer(appReducer)
      .expect(action)
      .toReturnState({ ...initialState });
  });
});

// unit test for selector
describe("store/selectors", () => {
  it("should get selected results properties when empty", () => {
    Selector(selectors.results)
      .expect(emptyState)
      .toReturn([]);
  });

  it("should get selected saved properties when full", () => {
    Selector(selectors.saved)
      .expect(stateSavedTwo)
      .toReturn(stateSavedTwo.saved);
  });
});

// tests for the entire store
describe("store", () => {
  let store;

  beforeEach(() => {
    jest.resetAllMocks();
    store = createStore(appReducer);
  });

  it("should be able to remove saved properties", () => {
    store.dispatch({ type: "REMOVE_PROPERTY", id: "4" });
    expect(selectors.results(store.getState())).toEqual(initialState.results);
    expect(selectors.saved(store.getState())).toEqual([]);
  });

  it("should be able to add properties", () => {
    store.dispatch({ type: "ADD_PROPERTY", id: "2" });
    expect(selectors.results(store.getState())).toEqual(stateSavedTwo.results);
    expect(selectors.saved(store.getState())).toEqual(stateSavedTwo.saved);
  });

  it("should still match the same state after messing around", () => {
    store.dispatch({ type: "ADD_PROPERTY", id: "1" });
    store.dispatch({ type: "REMOVE_PROPERTY", id: "1" });
    store.dispatch({ type: "ADD_PROPERTY", id: "2" });
    store.dispatch({ type: "ADD_PROPERTY", id: "2" });
    expect(selectors.results(store.getState())).toEqual(stateSavedTwo.results);
    expect(selectors.saved(store.getState())).toEqual(stateSavedTwo.saved);
  });
});
