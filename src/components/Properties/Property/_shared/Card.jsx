import React from "react";

class Card extends React.Component {
  state = {
    cardHover: false,
    buttonHover: false
  };
  handleCardMouseEnter = () => {
    this.setState({
      cardHover: true
    });
  };

  handleCardMouseLeave = () => {
    this.setState({
      cardHover: false
    });
  };

  handleButtonMouseEnter = () => {
    this.setState({
      buttonHover: true
    });
  };

  handleButtonMouseLeave = () => {
    this.setState({
      buttonHover: false
    });
  };

  render() {
    const { cardHover, buttonHover } = this.state;
    const {
      children,
      buttonText,
      buttonOnClick,
      style,
      ...otherProps
    } = this.props;
    const styles = {
      root: {
        fontFamily: "Source Sans Pro",
        position: "relative",
        boxShadow: cardHover
          ? "0 8px 28px rgba(0,0,0,0.25), 0 8px 10px rgba(0,0,0,0.22)"
          : "0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)",
        transition: "all 0.3s cubic-bezier(.25,.8,.25,1)"
      },
      button: {
        position: "absolute",
        right: 15,
        bottom: 20,
        borderRadius: 5,
        color: "#fff",
        textTransform: "upperCase",
        letterSpacing: 0.5,
        backgroundColor: (cardHover && buttonHover && "#8696B2") || "#AAB2C3",
        padding: "5px 12px 5px 12px",
        fontSize: 15,
        cursor: "pointer"
      }
    };

    return (
      <div
        onMouseEnter={() => this.handleCardMouseEnter()}
        onMouseLeave={() => this.handleCardMouseLeave()}
        style={{ ...styles.root, ...style }}
        {...otherProps}
      >
        {children}
        {cardHover && (
          <div
            onMouseEnter={() => this.handleButtonMouseEnter()}
            onMouseLeave={() => this.handleButtonMouseLeave()}
            style={styles.button}
            className="actionButton"
            onClick={e => buttonOnClick(e)}
          >
            {buttonText}
          </div>
        )}
      </div>
    );
  }
}

export default Card;
