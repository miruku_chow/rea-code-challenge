import React from "react";
import Card from "./_shared/Card.jsx";

const Property = ({
  price,
  agency,
  id,
  mainImage,
  addProperty,
  removeProperty
}) => (
  <Card
    style={styles.root}
    buttonText={
      addProperty ? "add property" : removeProperty ? "remove property" : null
    }
    buttonOnClick={
      addProperty ? (
        () => addProperty(id)
      ) : removeProperty ? (
        () => removeProperty(id)
      ) : null
    }
  >
    <div
      style={{
        backgroundColor: (agency && agency.brandingColors.primary) || "white",
        ...styles.header
      }}
    >
      {agency && <img style={styles.logo} src={agency.logo} />}
    </div>
    <img style={styles.image} src={mainImage} />
    <div style={styles.price} className="price">
      <div style={styles.price.text}>{price}</div>
    </div>
  </Card>
);

const styles = {
  root: {
    width: 420,
    height: 415,
    margin: "30px 0",
    display: "flex",
    flexDirection: "column"
  },
  logo: {
    display: "block",
    height: "100%"
  },
  header: {
    height: 30
  },
  image: {
    display: "block",
    width: "100%"
  },
  price: {
    display: "flex",
    flex: 1,
    alignItems: "center",
    text: {
      fontWeight: "bold",
      fontSize: 30,
      marginLeft: 15
    }
  }
};
export default Property;
