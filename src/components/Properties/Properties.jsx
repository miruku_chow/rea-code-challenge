import React from "react";
import Property from "./Property/Property.jsx";

const Properties = ({ header, properties, ...otherProps }) => (
  <div style={styles.root}>
    <div style={styles.header}>{header}</div>
    {properties && properties.length > 0 &&
      properties.map((item, i) => (
        <Property key={i} {...item} {...otherProps} />
      ))}
  </div>
);

const styles = {
  root: {
    padding: "0 15px",
  },
  header: {
    marginTop: 10,
    borderBottom: '1px solid #EBEBEB',
    width: 420,
    paddingBottom: 5,
    textTransform: "capitalize",
    fontFamily: "Source Sans Pro",
    fontWeight: "bold",
    fontSize: 25,
    letterSpacing: 0.5,
  }
};
export default Properties;
