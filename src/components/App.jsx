import React from "react";
import Properties from "./Properties/Properties.jsx";
import { connect } from "react-redux";
import {
  addProperty,
  removeProperty,
  appReducer,
  selectors
} from "../reducers/reducer.js";

export const App = ({ results, saved, addProperty, removeProperty }) => (
  <div style={styles.root}>
    <Properties
      properties={results}
      header="results"
      addProperty={addProperty}
    />
    <Properties
      properties={saved}
      header="saved properties"
      removeProperty={removeProperty}
    />
  </div>
);

const styles = {
  root: {
    display: "flex",
    maxWidth: 900,
    justifyContent: "flex-start",
    margin: "0 auto"
  }
};

export default connect(
  state => ({
    results: selectors.results(state),
    saved: selectors.saved(state)
  }),
  {
    addProperty: addProperty,
    removeProperty: removeProperty
  }
)(App);
