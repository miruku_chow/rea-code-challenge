// import React from 'react';
// import ReactDOM from 'react-dom';
// import App from './src/App.jsx';
 
// ReactDOM.render(<App />, document.getElementById('app'));

import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import { appReducer } from './reducers/reducer.js'
import App from './components/App.jsx';

let store = createStore(appReducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('app')
)